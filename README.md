djekyll
=======

Django implementation of [Jekyll](http://jekyllrb.com)

* __Authors__: [Ondrej Sika](http://ondrejsika.com/c.html)
* __GitHub__: <https://github.com/ondrejsika/djekyll>
