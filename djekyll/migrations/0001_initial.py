# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Layout'
        db.create_table(u'djekyll_layout', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('markdown', self.gf('django.db.models.fields.BooleanField')()),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'djekyll', ['Layout'])

        # Adding model 'Variable'
        db.create_table(u'djekyll_variable', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('key', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('value', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'djekyll', ['Variable'])

        # Adding model 'Page'
        db.create_table(u'djekyll_page', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('layout', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['djekyll.Layout'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('url', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('markdown', self.gf('django.db.models.fields.BooleanField')()),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'djekyll', ['Page'])

        # Adding model 'PageVariable'
        db.create_table(u'djekyll_pagevariable', (
            (u'variable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['djekyll.Variable'], unique=True, primary_key=True)),
            ('page', self.gf('django.db.models.fields.related.ForeignKey')(related_name='variable', to=orm['djekyll.Page'])),
        ))
        db.send_create_signal(u'djekyll', ['PageVariable'])

        # Adding model 'Post'
        db.create_table(u'djekyll_post', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('layout', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['djekyll.Layout'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('slug', self.gf('django.db.models.fields.SlugField')(max_length=50)),
            ('created', self.gf('django.db.models.fields.DateTimeField')()),
            ('markdown', self.gf('django.db.models.fields.BooleanField')()),
            ('body', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'djekyll', ['Post'])

        # Adding model 'PostVariable'
        db.create_table(u'djekyll_postvariable', (
            (u'variable_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['djekyll.Variable'], unique=True, primary_key=True)),
            ('post', self.gf('django.db.models.fields.related.ForeignKey')(related_name='variable', to=orm['djekyll.Post'])),
        ))
        db.send_create_signal(u'djekyll', ['PostVariable'])

        # Adding model 'File'
        db.create_table(u'djekyll_file', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('f', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
        ))
        db.send_create_signal(u'djekyll', ['File'])


    def backwards(self, orm):
        # Deleting model 'Layout'
        db.delete_table(u'djekyll_layout')

        # Deleting model 'Variable'
        db.delete_table(u'djekyll_variable')

        # Deleting model 'Page'
        db.delete_table(u'djekyll_page')

        # Deleting model 'PageVariable'
        db.delete_table(u'djekyll_pagevariable')

        # Deleting model 'Post'
        db.delete_table(u'djekyll_post')

        # Deleting model 'PostVariable'
        db.delete_table(u'djekyll_postvariable')

        # Deleting model 'File'
        db.delete_table(u'djekyll_file')


    models = {
        u'djekyll.file': {
            'Meta': {'object_name': 'File'},
            'f': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'djekyll.layout': {
            'Meta': {'object_name': 'Layout'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'markdown': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'djekyll.page': {
            'Meta': {'object_name': 'Page'},
            'body': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'layout': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djekyll.Layout']"}),
            'markdown': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'url': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'djekyll.pagevariable': {
            'Meta': {'object_name': 'PageVariable', '_ormbases': [u'djekyll.Variable']},
            'page': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'variable'", 'to': u"orm['djekyll.Page']"}),
            u'variable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['djekyll.Variable']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'djekyll.post': {
            'Meta': {'object_name': 'Post'},
            'body': ('django.db.models.fields.TextField', [], {}),
            'created': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'layout': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['djekyll.Layout']"}),
            'markdown': ('django.db.models.fields.BooleanField', [], {}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '50'})
        },
        u'djekyll.postvariable': {
            'Meta': {'object_name': 'PostVariable', '_ormbases': [u'djekyll.Variable']},
            'post': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'variable'", 'to': u"orm['djekyll.Post']"}),
            u'variable_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['djekyll.Variable']", 'unique': 'True', 'primary_key': 'True'})
        },
        u'djekyll.variable': {
            'Meta': {'object_name': 'Variable'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'key': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'value': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        }
    }

    complete_apps = ['djekyll']