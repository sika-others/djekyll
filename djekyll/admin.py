from django.contrib import admin
from models import Layout, Page, Post, File, PageVariable, PostVariable


class PageVariableInline(admin.TabularInline):
    model = PageVariable
    extra = 0

class PageAdmin(admin.ModelAdmin):
    inlines = (PageVariableInline, )

class PostVariableInline(admin.TabularInline):
    model = PostVariable
    extra = 0

class PostAdmin(admin.ModelAdmin):
    inlines = (PostVariableInline, )


admin.site.register(Layout)
admin.site.register(Page, PageAdmin)
admin.site.register(Post, PostAdmin)
admin.site.register(File)