import datetime

from django.shortcuts import render_to_response, get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.template import Template, Context
from django.utils.safestring import mark_safe

import markdown2

from .models import Page, Post

def render(page):
    if page.layout.markdown:
        template = markdown2.markdown(page.layout.body)
    else:
        template = page.layout.body
    if page.markdown:
        body = markdown2.markdown(page.body)
    else:
        body = page.body
    context = {
        "page": page,
        "content": mark_safe(body),
        "posts": Post.objects.filter(created__lte=datetime.datetime.now()).order_by("-created")
    }
    for variable in page.variable.all():
        context[variable.key] = variable.value
    template = Template(template).render(Context(context))
    template = Template(template)
    context = Context(context)
    return HttpResponse(template.render(context))

def page_view(request, url):
    page = get_object_or_404(Page, url="/"+url)
    return render(page)

def post_view(request, y, m, d, slug):
    page = get_object_or_404(Post, created__year=y, created__month=m, created__day=d, slug=slug)
    return render(page)
