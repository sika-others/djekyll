from django.db import models
from django.core.urlresolvers import reverse

MarkdownField = models.TextField

class Layout(models.Model):
    name = models.CharField(max_length=255)

    markdown = models.BooleanField()
    body = MarkdownField()

    def __unicode__(self):
        return u"%s" % self.name

class Variable(models.Model):
    key = models.CharField(max_length=64)
    value = models.CharField(max_length=255)

    def __unicode__(self):
        return u"%s=%s" % (self.key, self.value)

class Page(models.Model):
    layout = models.ForeignKey(Layout)

    name = models.CharField(max_length=255)
    url = models.CharField(max_length=255)

    markdown = models.BooleanField()
    body = MarkdownField(blank=True, default="")

    def __init__(self, *args, **kwargs):
        super(Page, self).__init__(*args, **kwargs)
        for variable in self.variable.all():
            setattr(self, variable.key, variable.value)

    def __unicode__(self):
        return u"%s %s" % (self.name, self.url)

class PageVariable(Variable):
    page = models.ForeignKey(Page, related_name="variable")

class Post(models.Model):
    layout = models.ForeignKey(Layout)

    name = models.CharField(max_length=255)
    slug = models.SlugField()
    created = models.DateTimeField()

    markdown = models.BooleanField()
    body = MarkdownField(blank=True, default="")

    def get_url(self):
        return reverse("djekyll:post", args=(self.created.year, self.created.month, self.created.day, self.slug))
    url = property(get_url)

    def __init__(self, *args, **kwargs):
        super(Post, self).__init__(*args, **kwargs)
        for variable in self.variable.all():
            setattr(self, variable.key, variable.value)

    def __unicode__(self):
        return u"%s" % self.name

class PostVariable(Variable):
    post = models.ForeignKey(Post, related_name="variable")

class File(models.Model):
    f = models.FileField(upload_to="djekyll/file/f")

    def __unicode__(self):
        return u"%s" % self.f.url