from django.conf.urls import patterns, include, url

urlpatterns = patterns("djekyll.views",
    url(r'^(?P<y>\d+)/(?P<m>\d+)/(?P<d>\d+)/(?P<slug>[\w-]+)$', 'post_view', name="post"),
    url(r'^(?P<url>.*)$', "page_view", name="page", ),
)